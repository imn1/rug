Exceptions
==========
The library comes with a few exceptions which are raised for
better error (what went wrong) distinguishion.

.. automodule:: rug.exceptions
   :members:
