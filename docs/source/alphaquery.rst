AlphaQuery
==========

Following methods are provided:

.. autoclass:: rug.alphaquery.AlphaQuery
   :members:
