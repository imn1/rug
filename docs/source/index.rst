Rug documentation
=================

Each provider is represented by it's own class. You can import it like

.. code-block:: python

   from rug import StockAnalysis

For what this library can do walk thru provider classes bellow.

.. toctree::
   :maxdepth: 3

   alphaquery
   barchart
   etfdb
   exceptions
   finviz
   stocktwits
   stockanalysis
   tipranks
   .. yahoo
