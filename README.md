<div align="center">
    <img src="https://gitlab.com/imn1/rug/-/raw/master/assets/logo.png">
</div>

<div align="center">
    <img src="https://img.shields.io/pypi/v/rug.svg?color=0c7dbe">
    <img src="https://img.shields.io/pypi/l/karpet.svg?color=0c7dbe">
    <img src="https://img.shields.io/pypi/dm/karpet.svg?color=0c7dbe">
</div>

# Rug

Universal library for fetching Stock and ETF data from the internet - mostly unofficial
APIs - no limits, more free data.

(for Cryptocurrency alternative see [karpet](https://gitlab.com/imn1/karpet))

* [PyPI](https://pypi.org/project/rug/)
* [documentation](https://rug.readthedocs.io/en/latest/) ![Documentation Status](https://readthedocs.org/projects/rug/badge/?version=latest)
* [changelog](./CHANGELOG.md)
