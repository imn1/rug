### 0.10.10

- dependencies updated

### 0.10.9

- better error handling (in general)

### 0.10.8

- fixed parsing tables (in general)

### 0.10.8

- fixed ``IndexError`` in ``TipRanks.get_current_price_change()`` method in case of invalid/unknown symbol

### 0.10.7

- `AlphaQuery.get_revenues()` and `AlphaQuery.get_earnings()` methods fixed
- Improved HTML table parsing
- `FinViz.get_insider_trading()` data sorted by date
- `StockAnalysis.get_etf_holdings()` fixed

### 0.10.6

- `EtfDb.get_basic_info()` -> `BarChart.get_etf_basic_info()` 
- `EtfDb.get_holdings()` -> `StockAnalysis.get_etf_holdings()`
- `EtfDb.get_etfs_by_item()` has been canceled (no equivalent)

### 0.10.5-post.1

- minor fixes from previous release

### 0.10.5

- method transition `Yahoo.get_current_price_change()` -> `TipRanks.get_current_price_change()`
- added parsing "today" as a date

### 0.10.4

- fixed unnecessary redirect for `Yahoo.get_current_price_change()` method

### 0.10.3

- fixed `TipRanks.get_basic_info()` method - random fails

### 0.10.2

- fixed `TipRanks.get_basic_info()` method - completely new data source
- fixed `TipRanks.get_dividends()` medhod - new data source

### 0.10.1

- fixed `FinViz.get_insider_trading()` method - wrong date parsing
- fixed `Yahoo.get_current_price_change()` method - wrong data digging

### 0.10

- new `EtfDb.get_etfs_by_item()` method
- CI added
- requirements updated
- following redirects for all requests

### 0.9.2
- fixed sorting for table structured data

### 0.9.1

- fixed `Yahoo.get_current_price_change()` return values - returns floats or `None` now 

### 0.9

- new `EtfDb.get_holdings()` method

### 0.8.5

- return back to Yahoo provider of `get_current_price_change()` method

### 0.8.4

- fixed market state determination for `get_current_price_change()`
  method

### 0.8.3

- rewritten `get_current_price_change()` method and
  moved from `Yahoo` to `StockAnalysis`

### 0.8.2

- (yanked)

### 0.8.1

- fixed `FinViz.get_price_ratings()` method

### 0.8

- new `FinViz.get_insider_trading()` method
- dependencies updated
- fixed dividends yield

### 0.7.2

- fixed raising IndexError instead of SymbolNotFound

### 0.7.1

- new properties for `EtfDb.get_basic_info()`

### 0.7

- new provider EtfDb.com

### 0.6

- new provider StockAnalysis.com

### 0.5.1

- better exception handling (distinction for not found symbols)

### 0.5

- new provider FinViz
- dependencies updated

### 0.4.2

- earnings -> eps
- added regular earnings method to AlphaQuery

### 0.4.1

- fixed revenues fetching

### 0.4

- new provider BarChart.com

### 0.3

- new provider AlphaQuery.com
- updated imports and class names - now it's i.e. `from rug import Yahoo`

### 0.2.11

- minor fixes

### 0.2.10

- minor fixes

### 0.2.9

- `yahoo.UnofficialAPI.get_current_price()` renamed to `get_current_price_change()`

### 0.2.8

- Added: ex-date is back for dividends

### 0.2.7

- Updated: better dividends handling (i.e. missing data)

### 0.2.6

- Added: exception handling

### 0.2.5

- Fixed TipTanks API - basic info for companies with no dividends

### 0.2.4

- Fixed TipRanks API - dividends for companies with no dividends

### 0.2.3

- Fixed TipRanks API - all methods

### 0.2.2

* Minor fixes.

### 0.2.1

Method `rug.yahoo.UnofficialAPI.get_current_price()` returns market state now.

### 0.2

New portals added: YAHOO! + StockTwits

* `get_current_price()` method added
* `get_earnings_calendar` method added

### 0.1.2
* `get_dividends()` now returns dividend `amount` too

### 0.1.1
* dates are now `datetime.date` instance

### 0.1
* initial release
