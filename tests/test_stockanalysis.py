import os
import sys

sys.path.insert(0, os.path.abspath("../rug"))


from rug import StockAnalysis


def test_get_etf_holdings():
    def do_test(symbol):
        api = StockAnalysis(symbol)
        holdings = api.get_etf_holdings()

        assert type(holdings) is list
        assert type(holdings[0][0]) is str
        assert type(holdings[0][1]) is str
        assert type(holdings[0][2]) is float
        assert type(holdings[0][3]) is float or "n/a" == holdings[0][3]

    do_test("splg")
    do_test("indy")
    do_test("tqqq")
    do_test("arkb")
