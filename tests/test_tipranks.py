import datetime
import os
import sys

import pytest

sys.path.insert(0, os.path.abspath("../rug"))

from rug import TipRanks
from rug.exceptions import SymbolNotFound


def test_get_dividends():
    api = TipRanks("AAPL")
    dividends = api.get_dividends()

    assert isinstance(dividends, list)
    assert list(dividends[0].keys()) == [
        "yield",
        "amount",
        "ex_date",
        "payment_date",
        "record_date",
        "growth_since",
    ]

    if dividends[0]["ex_date"]:
        assert isinstance(dividends[0]["ex_date"], datetime.date)

    if dividends[0]["payment_date"]:
        assert isinstance(dividends[0]["payment_date"], datetime.date)

    if dividends[0]["record_date"]:
        assert isinstance(dividends[0]["record_date"], datetime.date)

    if dividends[0]["growth_since"]:
        assert isinstance(dividends[0]["growth_since"], datetime.date)

    # No dividends.
    api = TipRanks("tsla")
    dividends = api.get_dividends()

    assert isinstance(dividends, list)
    assert dividends == []


def test_get_dividends_wrong_symbol():
    api = TipRanks("AAPLL")

    with pytest.raises(SymbolNotFound):
        api.get_dividends()


def test_get_basic_info():
    def test(info):
        assert isinstance(info, dict)
        for key in [
            "company_name",
            "market",
            "description",
            "has_dividends",
            "yoy_change",
            "year_low",
            "year_high",
            "pe_ratio",
            "eps",
            "market_cap",
            "similar_stocks",
        ]:
            assert key in info.keys()

        assert list(info["similar_stocks"][0].keys()) == [
            "ticker",
            "company_name",
        ]

    test(TipRanks("AAPL").get_basic_info())
    test(TipRanks("BABA").get_basic_info())
    test(TipRanks("META").get_basic_info())


def test_get_basic_info_wrong_symbol():
    api = TipRanks("AAPLL")

    with pytest.raises(SymbolNotFound):
        api.get_basic_info()

    api = TipRanks("CEZ.PR")

    with pytest.raises(SymbolNotFound):
        api.get_basic_info()
